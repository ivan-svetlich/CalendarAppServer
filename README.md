# PLANNER - Calendar App
This is the server's repository. See the [client's repository](https://github.com/ivan-svetlich/calendar-app-client).

## Table of contents
* [General Info](#general-info)
* [Technologies](#technologies)

## General Info
Web Api for the [PLANNER - Calendar App](https://github.com/ivan-svetlich/calendar-app-client) project.

## Technologies
### This project was designed with:
* C# .NET 5
* ASP.NET Core Identity
* JSON Web Tokens (jwt)
* MySQL
